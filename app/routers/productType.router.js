const express = require("express");
// import controllers
const { getAllProductType, createProductType, getProductTypeById, updateProductType, deleteProductType } = require("../controllers/productType.controller");
// import middlewares
const { productTypeMiddleware } = require("../middlewares/productType.middleware");
// khởi tạo router
const productTypeRouter = express.Router();

// router create product type
productTypeRouter.post("/product_types", productTypeMiddleware, createProductType);
// router get all product type: 
productTypeRouter.get("/product_types", productTypeMiddleware, getAllProductType);
// router get product type detail by ID
productTypeRouter.get("/product_types/:typeId", productTypeMiddleware, getProductTypeById);
// router update product type by ID
productTypeRouter.put("/product_types/:typeId", productTypeMiddleware, updateProductType);
// router update product type by ID
productTypeRouter.delete("/product_types/:typeId", productTypeMiddleware, deleteProductType);

module.exports = { productTypeRouter };