const express = require("express");
const { createOrderOfCustomer, getAllOrdersDatabase, getAllOrdersOfCustomer, getOrderDetailById, updateOrder, deleteOrder } = require("../controllers/order.controller");
const { orderMiddleware } = require("../middlewares/order.middleware");
const userMiddleware = require("../middlewares/user.middleware");

const orderRouter = express.Router();

// router create order of customer
orderRouter.post("/customers/:customerId/orders", userMiddleware.verifyToken, orderMiddleware, createOrderOfCustomer);

// router get all orders in database: 
orderRouter.get("/orders", userMiddleware.verifyTokenAndAdminRole, getAllOrdersDatabase);

// router get all orders of one customer: 
orderRouter.get("/customers/:customerId/orders", orderMiddleware, getAllOrdersOfCustomer);

// router get order detail by ID
orderRouter.get("/orders/:orderId", orderMiddleware, getOrderDetailById);

// router update order by ID
orderRouter.put("/orders/:orderId", userMiddleware.verifyTokenAndAdminRole, updateOrder);

// router delete order by ID
orderRouter.delete("/orders/:orderId", userMiddleware.verifyTokenAndAdminRole, deleteOrder);

module.exports = {orderRouter};