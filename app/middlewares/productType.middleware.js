const productTypeMiddleware = (req, res, next) => {
    console.log("PRODUCT-TYPE MIDDLEWARE - TIME: " + new Date().toLocaleString() + " - METHOD: " + req.method + " - URL: " + req.url);
    next();
}

module.exports = { productTypeMiddleware };