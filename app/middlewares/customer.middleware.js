const customerMiddleware = (req, res, next) => {
    console.log("CUSTOMER MIDDLEWARE - TIME: " + new Date().toLocaleString() + " - METHOD: " + req.method + " - URL: " + req.url);
    next();
}

module.exports = { customerMiddleware };