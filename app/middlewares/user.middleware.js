const jwt = require("jsonwebtoken");

const userMiddleware = {
    // VERIFY TOKEN
    verifyToken: (req, res, next) => {
        const token = req.headers.token;
        if (token) {
            const accessToken = token.split(" ")[1];
            jwt.verify(accessToken, process.env.JWT_ACCESS_KEY, (err, user) => {
                if (err) {
                    return res.status(403).json("Access Token không hợp lệ");
                }
                else {
                    req.user = user;
                    next();
                }
            })
        }
        else {
            return res.status(401).json("Access Token không tồn tại");
        }
    },
    // VERIFY TOKEN AND ROLE ADMIN
    verifyTokenAndAdminRole: (req, res, next) => {
        userMiddleware.verifyToken(req, res, () => {
            if (req.user.role === "admin") {
                next();
            }
            else {
                return res.status(403).json("Role không hợp lệ");
            }           
        })
    }
}

module.exports = userMiddleware;