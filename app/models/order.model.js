const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    orderCode: {
        type: String,
        required: true,
        unique: true
    },
    orderDate: {
        type: String
    },
    shippedDate: {
        type: String,
        default: ""
    },
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    address: {
        type: String,
    },
    city: {
        type: String,
    },
    district: {
        type: String,
    },
    ward: {
        type: String,
    },
    note: {
        type: String,
        default: ""
    },
    orderDetails: [{
        type: mongoose.Types.ObjectId,
        ref: "order_detail"
    }],
    status: {
        type: String,
        default: "New"
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("order", orderSchema);