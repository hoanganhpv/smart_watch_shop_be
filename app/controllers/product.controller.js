const mongoose = require("mongoose");
const productModel = require("../models/product.model");

// Create product
const createProduct = async (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    // check name
    if (body.name === undefined || body.name.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name is invalid"
        });
    }
    // check description
    if (body.description !== undefined && body.description.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Description is invalid"
        });
    }
    // check type
    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type is invalid"
        });
    }
    // check imageUrl
    if (body.imageUrl === undefined || body.imageUrl.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Image URL is invalid"
        });
    }
    // check buyPrice
    if (!Number.isInteger(body.buyPrice) || body.buyPrice < 0) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Buy Price is invalid"
        });
    }
    // check promotionPrice
    if (!Number.isInteger(body.promotionPrice) || body.promotionPrice < 0) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Promotion Price is invalid"
        });
    }
    // check amount
    if (!Number.isInteger(body.amount) || body.amount < 0) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Amount is invalid"
        });
    }

    // B3: Gọi model và xử lý CSDL
    let newProduct = {
        name: body.name,
        description: body.description,
        article: body.article,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    try {
        const data = await productModel.create(newProduct);
        return res.status(201).json({
            status: "Create Product successful",
            data
        });
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

// Get all products
const getAllProducts = async (req, res) => {
    // B1: Thu thập dữ liệu
    const { name, brand, min, max } = req.query;
    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.skip);
    let filterData = {};
    // nếu có query tên sẩn phẩm: tìm trong database tên sp có chứa giá trị của query name, ko phân biệt chữ hoa
    if (name) {
        filterData.name = { $regex: name, $options: 'i' };
    }

    if (brand) {
        filterData.type = { $in: brand };
    }

    if (min && max) {
        filterData.promotionPrice = { $gte: min, $lte: max };
    } 
    else if (min) {
        filterData.promotionPrice = { $gte: min };
    } 
    else if (max) {
        filterData.promotionPrice = { $lte: max };
    }
    // B2: Kiểm tra dữ liệu
    // B3: Gọi model và xử lý CSDL
    try {
        // If not enter limit: return all products
        if (!limit) {
            const data = await productModel.find(filterData);
            return res.status(200).json({
                status: "Get All Product successful",
                data
            });
        }
        else {
            const products = await productModel.find(filterData).skip(skip).limit(limit);
            return res.status(200).json({
                status: "Get Producs successful",
                data: products
            });
        }
    }
    catch (err) {
        return res.status(500).json({
            status: "Server Internal Error",
            err
        });
    }
}

// Get product detail by ID
const getProductById = async (req, res) => {
    // B1: Thu thập dữ liệu
    let productId = req.params.productId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    try {
        const data = await productModel.findById(productId);
        return res.status(200).json({
            status: "Get Product Detail successful",
            data
        });
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

// Update product by ID
const updateProduct = (req, res) => {
    // B1: Thu thập dữ liệu
    let productId = req.params.productId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type is invalid"
        });
    }
    // check if name is exist but not valid
    if (body.name !== undefined && body.name.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name is invalid"
        });
    }
    // check if description is exist but not valid
    if (body.description !== undefined && body.description.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Description is invalid"
        });
    }
    // check if type is exist but not valid
    if (body.type !== undefined && !mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type is invalid"
        });
    }
    // check if imageUrl is exist but not valid
    if (body.imageUrl !== undefined && body.imageUrl.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Image URL is invalid"
        });
    }
    // check if buyPrice is exist but not valid
    if (body.buyPrice !== undefined && (!Number.isInteger(body.buyPrice) || body.buyPrice < 0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Buy Price is invalid"
        });
    }
    // check if promotionPrice is exist but not valid
    if (body.promotionPrice !== undefined && (!Number.isInteger(body.promotionPrice) || body.promotionPrice < 0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Promotion Price is invalid"
        });
    }
    // check if amount is exist but not valid
    if (body.amount !== undefined && (!Number.isInteger(body.amount) || body.amount < 0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Amount is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    let updateProduct = {
        name: body.name,
        description: body.description,
        article: body.article,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    try {
        const data = productModel.findByIdAndUpdate(productId, updateProduct);
        return res.status(200).json({
            status: "Update Product successful",
            data
        });
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

// Delete product by ID
const deleteProduct = async (req, res) => {
    // B1: Thu thập dữ liệu
    let productId = req.params.productId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    try {
        await productModel.findByIdAndDelete(productId);
        return res.status(204).json();
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

module.exports = { createProduct, getAllProducts, getProductById, updateProduct, deleteProduct }