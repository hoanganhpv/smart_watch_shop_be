const mongoose = require("mongoose");
const randToken = require("rand-token");
const orderModel = require("../models/order.model");
const orderDetailModel = require("../models/orderDetail.model");
const customerModel = require("../models/customer.model");

// Create order of a customer
const createOrderOfCustomer = async (req, res) => {
    try {
        // B1: Thu thập dữ liệu
        let customerId = req.params.customerId;
        let body = req.body;
        let token = randToken.generate(8);

        // B2: Kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(customerId)) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Customer ID is invalid"
            });
        }
        // check shippedDate
        if (body.shippedDate !== undefined && body.shippedDate.trim() === "") {
            return res.status(400).json({
                status: "Bad Request",
                message: "Shipped Date is invalid"
            });
        }
        // B3: Gọi model và xử lý CSDL
        let newOrder = {
            orderCode: token,
            orderDate: new Date().toLocaleString("en-gb"),
            fullName: body.fullName,
            phone: body.phone,
            email: body.email,
            address: body.address,
            city: body.city,
            district: body.district,
            ward: body.ward,
            note: body.note
        };
        // Create new order:
        const order = await orderModel.create(newOrder);
        // Add order objectId to customer:
        await customerModel.findByIdAndUpdate(customerId, { $push: { orders: order._id } });
        return res.status(201).json(order);
    }
    catch (err) {
        return res.status(500).json(err);
    }
}

// Get all orders in database
const getAllOrdersDatabase = async (req, res) => {
    try {
        // B1: Thu thập dữ liệu
        let limit = parseInt(req.query.limit);
        let skip = parseInt(req.query.skip);
        // B2: Kiểm tra dữ liệu
        // B3: Gọi model và xử lý CSDL
        if (!limit) {
            const allOrders = await orderModel
                .find()
                .sort({ createdAt: -1 })
                .populate({
                    path: 'orderDetails',
                    populate: {
                        path: 'product'
                    }
                });
            return res.status(200).json(allOrders);
        }
        else {
            const allOrders = await orderModel
                .find()
                .sort({ createdAt: -1 })
                .populate({
                    path: 'orderDetails',
                    populate: {
                        path: 'product'
                    }
                })
                .skip(skip)
                .limit(limit);
            return res.status(200).json(allOrders);
        }
    }
    catch (err) {
        return res.status(500).json(err);
    }
}

// // Get all orders of a customer
const getAllOrdersOfCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Customer ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    customerModel.findById(customerId).populate("orders").exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get All Orders of Customer successful",
                data: data.orders
            });
        }
    })
}

// Get order detail by ID
const getOrderDetailById = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    orderModel.findById(orderId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get Order Detail successful",
                data
            });
        }
    })
}

// Update order by ID
const updateOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order ID is invalid"
        });
    }
    // check if shippedDate exist but not valid
    if (body.shippedDate !== undefined && body.shippedDate.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Shipped Date is invalid"
        });
    }
    // check if note exist but not valid
    if (body.note !== undefined && body.note.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order Date is invalid"
        });
    }

    // B3: Gọi model và xử lý CSDL
    let updateOrder = {
        shippedDate: body.shippedDate,
        note: body.note,
        status: body.status,
    }
    orderModel.findByIdAndUpdate(orderId, updateOrder, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json(data);
        }
    })
}

// Delete order by ID
const deleteOrder = async (req, res) => {
    try {
        // B1: Thu thập dữ liệu
        let orderId = req.params.orderId;
        // B2: Kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(orderId)) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Order ID is invalid"
            });
        }

        // B3: Gọi model và xử lý CSDL
        // Xóa tất cả order detail trong collection orderDetail:
        const order = await orderModel.findById(orderId);
        for await (let orderDetail of order.orderDetails) {
            await orderDetailModel.findByIdAndDelete(orderDetail._id);
        }
        // Xóa order trong collection user
        await customerModel.findOneAndUpdate({email: order.email}, { $pull: { orders: orderId } });
        // Xóa order
        await orderModel.findByIdAndDelete(orderId);
        
        return res.status(204).json("Delete Successful");
    }
    catch (err) {
        return res.status(500).json({
            status: "Server Internal Error",
            message: err
        });
    }

}

module.exports = { createOrderOfCustomer, getAllOrdersDatabase, getAllOrdersOfCustomer, getOrderDetailById, updateOrder, deleteOrder }