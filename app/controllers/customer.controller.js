const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const customerModel = require("../models/customer.model");
const userModel = require("../models/user.model");

// create customer
const createNewCustomer = async (req, res) => {
    // check fullName
    if (!req.body.fullName) {
        return res.status(400).json({
            message: "Full Name is invalid"
        });
    }
    // check phone
    if (!req.body.phone) {
        return res.status(400).json({
            message: "Phone is invalid"
        });
    }
    // check email
    if (!req.body.email) {
        return res.status(400).json({
            message: "Email is invalid"
        });
    }
    try {
        // CHECK BACKEND
        // check email đã tồn tại chưa
        let emailExist = await customerModel.findOne({ email: req.body.email });
        if (emailExist) {
            return res.status(500).json({
                message: "Email đã tồn tại"
            })
        }
        // check username đã tồn tại chưa
        let usernameExist = await userModel.findOne({ username: req.body.username });
        if (usernameExist) {
            return res.status(500).json({
                message: "Tên đăng nhập đã tồn tại"
            })
        }
        // nếu username và email chưa tồn tại thì tiếp tục
        if (!emailExist && !usernameExist) {
            // tạo user login mới của customer:
            const saltRounds = 10;
            const userPassword = req.body.password;
            const hash = await bcrypt.hash(userPassword, saltRounds);
            let newUser = {
                username: req.body.username,
                password: hash
            };
            let savedUser = await userModel.create(newUser);
            // tạo customer mới với thông tin user vừa tạo
            let newCustomer = {
                fullName: req.body.fullName,
                phone: req.body.phone,
                email: req.body.email,
                address: req.body.address,
                city: req.body.city,
                district: req.body.district,
                ward: req.body.ward,
                user: savedUser._id
            };
            let savedCustomer = await customerModel.create(newCustomer);
            return res.status(201).json(savedCustomer)
        }

    }
    catch (err) {
        return res.status(500).json(err);
    }
}

// Get all customers
const getAllCustomers = async (req, res) => {
    // B1: Thu thập dữ liệu
    const skip = req.query.skip;
    const limit = req.query.limit;
    // B2: Kiểm tra dữ liệu
    // B3: Gọi model và xử lý CSDL
    try {
        // nếu không có limit query thì trả về tất cả dữ liệu
        if (!limit) {
            const allCustomers = await customerModel.find().populate("user orders");
            return res.status(200).json(allCustomers);
        }
        else {
            const allCustomers = await customerModel.find().skip(skip).limit(limit).populate("user orders");
            return res.status(200).json(allCustomers);
        }
    }
    catch (err) {
        return res.status(500).json({
            message: err.message
        });
    }
}

// Get customer detail by phone, email or username:
const getCustomerInfo = async (req, res) => {
    try {
        // get username or email from query
        let username = req.query.username;
        let email = req.query.email;
        let phone = req.query.phone;
        if (username) {
            // find user by username
            const user = await userModel.findOne({ username: username });
            // then find customer by ID of username
            const customer = await customerModel.findOne({ user: user._id });
            return res.status(200).json(customer);
        }
        if (email) {
            // find customer by email
            const customer = await customerModel.findOne({ email: email }).populate("user");
            return res.status(200).json(customer);
        }
        if (phone) {
            // find customer by phone number
            const customer = await customerModel.findOne({ phone: phone }).populate("user");
            return res.status(200).json(customer);
        }
    }
    catch (err) {
        return res.status(500).json(err);
    }
}

// Get customer detail by ID
const getCustomerDetailById = async (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Customer ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    try {
        const customer = await customerModel.findById(customerId).populate("user");
        return res.status(200).json(customer);
    }
    catch (err) {
        return res.status(500).json(err);
    }
}

// Update customer
const updateCustomer = async (req, res) => {
    try {
        // B1: Thu thập dữ liệu
        let customerId = req.params.customerId;
        let body = req.body;
        // B2: Kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(customerId)) {
            return res.status(400).json({ message: "Customer ID is invalid" });
        }
        // check if fullName is exist but not valid
        if (body.fullName && body.fullName.trim() === "") {
            return res.status(400).json({ message: "Full Name is invalid" });
        }
        // check if phone is exist but not valid
        if (body.phone && body.phone.trim() === "") {
            return res.status(400).json({ message: "Phone is invalid" });
        }
        // check if email is exist but not valid
        if (body.email && body.email.trim() === "") {
            return res.status(400).json({ message: "Email is invalid" });
        }
        // check if address is exist but not valid
        if (body.address && body.address.trim() === "") {
            return res.status(400).json({ message: "Address is invalid" });
        }
        // check if city is exist but not valid
        if (body.city && body.city.trim() === "") {
            return res.status(400).json({ message: "City is invalid" });
        }
        // check if district is exist but not valid
        if (body.district && body.district.trim() === "") {
            return res.status(400).json({ message: "District is invalid" });
        }
        // check if ward is exist but not valid
        if (body.ward && body.ward.trim() === "") {
            return res.status(400).json({ message: "Ward is invalid" });
        }
        // B3: Gọi model và xử lý CSDL
        let updateCustomerData = {
            fullName: body.fullName,
            phone: body.phone,
            email: body.email,
            address: body.address,
            city: body.city,
            district: body.district,
            ward: body.ward,
        };
        const updateCustomer = await customerModel.findByIdAndUpdate(customerId, updateCustomerData);
        return res.status(200).json(updateCustomer);
    }
    catch (err) {
        return res.status(500).json(err);
    }
}

// delete customer
const deleteCustomer = async (req, res) => {
    try {
        // B1: Thu thập dữ liệu
        let customerId = req.params.customerId;
        // B2: Kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(customerId)) {
            return res.status(400).json({message: "Customer ID is invalid"});
        }
        // B3: Gọi model và xử lý CSDL
        const customer = await customerModel.findById(customerId);
        // delete user của customer:
        await userModel.findByIdAndDelete(customer.user._id);
        // lấy order của customer và delete:
        const ordersOfCustomer = customer.orders;
    }
    catch (err) {
        return res.status(500).json(err);
    }
}

module.exports = { createNewCustomer, getAllCustomers, getCustomerDetailById, updateCustomer, deleteCustomer, getCustomerInfo }