const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const userModel = require("../models/user.model");
const customerModel = require("../models/customer.model");

// Create refresh token list for global use
var refreshTokenList = [];

const userController = {
    // GENERATE ACCESS TOKEN
    generateAccessToken: (paramUser) => {
        return jwt.sign({
            id: paramUser.id,
            username: paramUser.username,
            role: paramUser.role,
        }, process.env.JWT_ACCESS_KEY, { expiresIn: "30m" });
    },

    // GENERATE REFRESH TOKEN
    generateRefreshToken: (paramUser) => {
        return jwt.sign({
            id: paramUser.id,
            username: paramUser.username,
            role: paramUser.role,
        }, process.env.JWT_REFRESH_KEY, { expiresIn: "30d" });
    },

    // LOGIN USER
    loginUser: async (req, res) => {
        try {
            // check username
            const user = await userModel.findOne({ username: req.body.username });
            if (!user) {
                return res.status(404).json("Sai tên đăng nhập");
            }
            // check password
            const reqPassword = req.body.password;
            const userPassword = user.password;
            const validPassword = await bcrypt.compare(reqPassword, userPassword);
            if (!validPassword) {
                return res.status(404).json("Sai mật khẩu");
            }
            // if username and password correct: LOGIN SUCCESS
            if (user && validPassword) {
                const customer = await customerModel.findOne({ user: user._id });
                // Generate Access Token and Refresh Token for user/customer
                const accessToken = userController.generateAccessToken(user);
                const refreshToken = userController.generateRefreshToken(user);
                // save refresh token to list
                refreshTokenList.push(refreshToken);
                // Save refresh token to cookies
                res.cookie("refreshToken", refreshToken, {
                    httpOnly: true,
                    secure: false,   // change value to true when deploy
                    sameSite: "strict"
                });
                // set data to be responsed to client
                const loginDataRes = {
                    id: user.id,
                    role: user.role,
                    customerId: customer.id,
                    fullName: customer.fullName,
                    profilePic: customer.profilePic,
                    accessToken,
                };
                // return response
                return res.status(200).json(loginDataRes);
            }
        }
        catch (err) {
            return res.status(500).json(err);
        }
    },

    // REFRESH TOKEN
    refreshToken: async (req, res) => {
        // get refresh token of client
        const refreshToken = req.cookies.refreshToken;
        if (!refreshToken) {
            return res.status(401).json("Refresh token không tồn tại");
        }
        if (!refreshTokenList.includes(refreshToken)) {
            return res.status(403).json("Refresh Token không hợp lệ");
        }
        // verify refresh token
        jwt.verify(refreshToken, process.env.JWT_REFRESH_KEY, (err, data) => {
            if (err) {
                console.log(err);
            }
            // remove old refresh token from list
            refreshTokenList = refreshTokenList.filter(token => token !== refreshToken);
            // create new access token and refresh token
            const newAccessToken = userController.generateAccessToken(data);
            const newRefreshToken = userController.generateRefreshToken(data);
            // save new refresh token to list
            refreshTokenList.push(newRefreshToken);
            // Save refresh token to cookies
            res.cookie("refreshToken", newRefreshToken, {
                httpOnly: true,
                secure: false,  // change value to true when deploy
                sameSite: "strict"
            });
            console.log(refreshTokenList);
            return res.status(200).json({accessToken: newAccessToken});
        })
    },

    // LOGOUT USER
    logoutUser: async (req, res) => {
        const refreshToken = req.cookies.refreshToken;
        await res.clearCookie("refreshToken");
        refreshTokenList = refreshTokenList.filter(token => token !== refreshToken);
        return res.status(200).json("Đã đăng xuất !");
    }
}

module.exports = userController;